#ifndef MAIN_H
#define MAIN_H

#include <QAbstractButton>
#include <QDesktopWidget>
#include <QSharedMemory>
#include <QApplication>
#include <QMessageBox>
#include <QTranslator>
#include <QFileInfo>
#include <QSettings>
#include <QProcess>
#include <QString>
#include <QThread>
#include <QDebug>
#include <QDir>

namespace xin {

enum Language {
    simplifiedChinese,
    English

};
//以下内容注意自修改
static QString exeName = "XiaoXin";
static QString versions = "1.0.0";
}

#endif // MAIN_H
