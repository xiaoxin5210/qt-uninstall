#ifndef UNINSTALLWIDGET_H
#define UNINSTALLWIDGET_H

#include <QWidget>

namespace Ui {
class uninstall;
}

class uninstall : public QWidget
{
    Q_OBJECT
public:
    explicit uninstall(QWidget *parent = nullptr);
public:
    void init();
private:
    QStringList getFiles(QString path);
    void delRegedit();
signals:
    void signalRemove(QString path);
public slots:
    void updateWidget(double data);
    void onError(QString error);
private:
    Ui::uninstall *ui;
    QStringList files;
};
static QString previousUnExePath;
int entry(int argc, char *argv[]);
#endif // UNINSTALLWIDGET_H
