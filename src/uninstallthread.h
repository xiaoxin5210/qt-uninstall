#ifndef UNINSTALLTHREAD_H
#define UNINSTALLTHREAD_H

#include <QObject>

class uninstallThread : public QObject
{
    Q_OBJECT
public:
    explicit uninstallThread(QStringList uninstallFiles, QObject *parent = nullptr);
signals:
    void signalUpdate(double data);
    void signalError(QString error);
public slots:
    void removeFile(QString path);
private:
    double value;
    double size;
    QStringList uninstallFiles;
};

#endif // UNINSTALLTHREAD_H
