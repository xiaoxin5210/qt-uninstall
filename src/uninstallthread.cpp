#include "uninstallthread.h"
#include "main.h"

uninstallThread::uninstallThread(QStringList uninstallFiles, QObject *parent)
    : QObject(parent)
    , size(0)
    , uninstallFiles(uninstallFiles)
{
    value = 100.0 / uninstallFiles.size(); //100/文件数量=每次更新大小
}

void uninstallThread::removeFile(QString path)
{
    QThread::msleep(10); //增加延时效果
    if (QFileInfo(path).exists()) {
        if (QFileInfo(path).isDir()) { //如果是文件夹
            if (QDir().rmdir(path)) {
                emit signalUpdate(size += value);
                return;
            }
            else {
                emit signalError(tr("文件夹%1删除失败,请手动删除!").arg(path));
                return;
            }
        }
        else { //是文件
            if (QDir().remove(path)) {
                emit signalUpdate(size += value);
            }
            else {
                emit signalError(tr("文件%1删除失败,请手动删除!").arg(path));
                return;;
            }
        }
    }
    else {
        emit signalError(tr("文件%1不存在,卸载失败!").arg(path));
        return;
    }
}
