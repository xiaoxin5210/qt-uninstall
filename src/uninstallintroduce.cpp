#include "main.h"
#include "uninstallintroduce.h"
#include "ui_uninstallintroduce.h"
#include "uninstall.h"

static bool isUnInstall = false;

UninstallIntroduce::UninstallIntroduce(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::UninstallIntroduce)
{
    ui->setupUi(this);
    init();
}

UninstallIntroduce::~UninstallIntroduce()
{
    delete ui;
}

void UninstallIntroduce::init()
{
    //读取注册表
    QString uninstallPath;
    QSettings settingsUninstall("HKEY_LOCAL_MACHINE\\SOFTWARE\\WOW6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\" + xin::exeName,
                                QSettings::NativeFormat);

    if (settingsUninstall.contains("InstallLocation")) {
        uninstallPath = settingsUninstall.value("InstallLocation").toString();
    }
    else {
        QMessageBox msgBox;
        msgBox.setText(tr("安装路径获取失败,请手动删除!"));
        msgBox.exec();
        exit(0);
    }
    isUnInstall = true;

    ui->widget->setStyleSheet("QWidget#widget{background-color: rgb(255, 255, 255);"
                              "border:1px solid rgb(173, 173, 173);}");

    ui->lineEdit->setText(uninstallPath);
    ui->lineEdit->setDisabled(true);

    ui->pushButtonUn->setText(tr("解除安装(U)"));
    ui->pushButtonUn->setStyleSheet("QPushButton{border: 1.5px solid rgb(0,120,215);"
                                    "background-color: rgb(229, 241, 251);}"
                                    "QPushButton:hover{border: 1px solid rgb(0,120,215);}");
    ui->pushButtonCancel->setText(tr("取消(C)"));
    ui->pushButtonCancel->setShortcut(Qt::Key_C);
    ui->pushButtonUn->setShortcut(Qt::Key_U);
    connect(ui->pushButtonUn, SIGNAL(clicked(bool)), this, SLOT(onButtonUn()));
    connect(ui->pushButtonCancel, SIGNAL(clicked(bool)), this, SLOT(onButtonCancel()));

    QFont f("Timers", 10);
    ui->label1->setFont(QFont("Timers",10, QFont::Bold));
    ui->label1->setText(tr("解除安装 %1 %2").arg(xin::exeName).arg(xin::versions));
    ui->label2->setFont(f);
    ui->label2->setText(tr("  从你的计算机解除安装 %1 %2").arg(xin::exeName).arg(xin::versions));
    ui->label3->setFont(f);
    ui->label3->setText(tr("这个向导将从你的计算机解除安装 %1 %2 , 单击 [解除安装(U)] \n开始解除安装进程.").arg(xin::exeName).arg(xin::versions));
    ui->labelDir->setFont(f);
    ui->labelDir->setText(tr("解除安装目录:"));
    ui->labelIco->setPixmap(QPixmap(":/xin.ico"));
    ui->labelIco->setScaledContents(true);
}

void UninstallIntroduce::onButtonUn()
{
    uninstall* u = new uninstall();
    QDesktopWidget* desktop = QApplication::desktop(); // =qApp->desktop();也可以
    u->move((desktop->width() - u->width()) / 2, (desktop->height() - u->height()) / 2);
    u->init();
    if (isUnInstall) {
        u->show();
        this->hide();
    }
}

void UninstallIntroduce::onButtonCancel()
{
    exit(0);
}
