#ifndef UNINSTALLINTRODUCE_H
#define UNINSTALLINTRODUCE_H

#include <QWidget>

namespace Ui {
class UninstallIntroduce;
}

class UninstallIntroduce : public QWidget
{
    Q_OBJECT

public:
    explicit UninstallIntroduce(QWidget *parent = nullptr);
    ~UninstallIntroduce();
private:
    void init();
private slots:
    void onButtonUn();
    void onButtonCancel();
private:
    Ui::UninstallIntroduce *ui;
};

#endif // UNINSTALLINTRODUCE_H
